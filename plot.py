#!/usr/bin/env python
from pylab import *
from mpl_toolkits.mplot3d import axes3d
import numpy as np
from staticbs.common import mag


def plot_linearity(B_fn, obs_points_fn="./data/obs_points_linearity.npy"):
    obs_points = np.load(obs_points_fn)
    B = np.load(B_fn)

    x = obs_points[:, 0]
    y = obs_points[:, 1]
    z = obs_points[:, 2]

    plt.subplot(2, 1, 1)
    plt.title('B field as a function of the X position')
    plt.xlabel('x position when y = 0')
    plt.plot(x[0:x.shape[0]/2], B[0:x.shape[0]/2, 0], '.')

    plt.subplot(2,1,2)
    plt.title('B field as a function of the y position')
    plt.xlabel('y position when x = 0')
    plt.plot(y[y.shape[0]/2:-1], B[y.shape[0]/2:-1, 1], '.')

    # ax4.scatter(x, y)

    # ax4.quiver(x, y,
    #        B[:, 0],
    #        B[:, 1], color='blue')


def scatter_3D(B_fn, obs_points_fn="data/obs_points.npy"):
    """Plot a scatter graph in 3D of the B field"""
    obs_points = np.load(obs_points_fn)
    B = np.load(B_fn)

    xx = obs_points[:, :, 0]
    yy = obs_points[:, :, 1]
    zz = obs_points[:, :, 2]

    fig = plt.figure()
    ax = fig.gca(projection='3d')

    sc = ax.scatter(xx, yy, zz, c=mag(B))
    plt.colorbar(cs)


def plot_3D(B_fn, obs_points_fn="./data/obs_points.npy"):
    """Plot a 3D vector graph of the B field"""
    obs_points = np.load(obs_points_fn)
    B = np.load(B_fn)

    xx = obs_points[:, :, 0]
    yy = obs_points[:, :, 1]
    zz = obs_points[:, :, 2]

    fig3 = plt.figure()
    ax3 = fig3.gca(projection='3d')

    ax3.quiver(xx[::4, ::4], yy[::4, ::4], zz[::4, ::4],
           B[::4, ::4, 0]/float(np.amax(B[::4, ::4, 0])),
           B[::4, ::4, 1]/float(np.amax(B[::4, ::4, 1])),
           B[::4, ::4, 2]/float(np.amax(B[::4, ::4, 2])), color='blue', length=0.001)


def plot_B(B_fn, obs_points_fn="./data/obs_points.npy"):
    """Plot of 2 vector graph of the B field"""
    obs_points = np.load(obs_points_fn)
    B = np.load(B_fn)

    xx = obs_points[:, :, 0]
    yy = obs_points[:, :, 1]
    zz = obs_points[:, :, 2]

    x = xx[0, :]
    y = yy[:, 0].T

    fig1 = plt.figure()
    ax1 = fig1.gca()
    title_txt = 'XY View of the triangle B Field'
    ax1.set_title(title_txt)
    ax1.CS = contourf(x, y, mag(B))
    ax1.quiver(xx[::4, ::4], yy[::4, ::4],
           B[::4, ::4, 0],
           B[::4, ::4, 1], color='white')
    plt.colorbar(ax1.CS)


def plot_E(E_fn, obs_points_fn="./data/obs_points.npy"):
    """Plot of 2 vector graph of the E field"""
    obs_points = np.load(obs_points_fn)
    E = np.load(E_fn)

    xx = obs_points[:, :, 0]
    yy = obs_points[:, :, 1]
    zz = obs_points[:, :, 2]

    x = xx[0, :]
    y = yy[:, 0].T

    fig2 = plt.figure()
    ax2 = fig2.gca()
    title_txt2 = 'XY View of the triangle E Field'
    ax2.set_title(title_txt2)
    ax2.CS = contourf(x, y, mag(E))
    ax2.quiver(xx[::4, ::4], yy[::4, ::4],
           E[::4, ::4, 0],
           E[::4, ::4, 1], color='white')
    plt.colorbar(ax2.CS)


def plot_coil(coil):
    """Plot the vertices of a coil in a 3D graph"""
    
    vertices = coil.vertices
    xx = vertices[:, 0]
    yy = vertices[:, 1]
    zz = vertices[:, 2]

    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.plot(xx, yy, zz, ":o")


if __name__ == '__main__':

    # plot_B(sys.argv[1])
    # plot_E(sys.argv[2])
    # plot_3D(sys.argv[1])
    scatter_3D(sys.argv[1])
    # plot_linearity(sys.argv[1])
    
    plt.show()