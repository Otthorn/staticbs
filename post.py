#! /bin/env python3
# author : Solal Nathan

import numpy as np
from staticbs.common import mag

obs_points = np.load('./data/obs_points.npy')
B = np.load('./data/B_test.npy')

norm_B = mag(B)


