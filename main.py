#!/usr/bin/env python
from pylab import *
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import staticbs as bs
from plot import *


def save(data_dir='./data/', **kwargs):
    for fn in kwargs:
        print((fn, kwargs[fn]))
        np.save(data_dir + fn, kwargs[fn])


# print dir(static_bs_module)

# observation grid setup

# grid units are in meters
x = linspace(-0.1, 0.1, 100)
y = linspace(-0.1, 0.1, 100)

enable_3D = False

# ====2D case====
if not enable_3D:
    # Example for how to ignore the z axis
    z = zeros((x.shape[0])) + 1e-2
    zz = zeros(x.shape + y.shape) + 1e-2
    xx, yy = meshgrid(x, y)

# ====3D case====
else:
    z = linspace(-2, 2, 20)
    xx, yy, zz = meshgrid(x, y, z)

observation_points = rollaxis(np.array((xx, yy, zz)), 0, xx.ndim+1)

# current and amp turns definitions

simulation_current = 10  # amps
num_loops = 1000

amp_turns = simulation_current * num_loops

# OPTIONAL water / charged particle velocity for e field calculation
velocity = np.array([0.001 ,0, 0])  # m/s

# create coil
coil_radius = 0.02  # meters
# coil_A = bs.shapes.Polygon(4, (-1,-1,0), coil_radius)
# coil_B = bs.shapes.Polygon(4, (1,1,0), coil_radius)

coil_A = bs.shapes.RectangularCoil((-0.04, 0, 0), coil_radius)
coil_B = bs.shapes.RectangularCoil((0.04, 0, 0), coil_radius)


# make a list of all of the current segments
# current_A = bs.iobjs.current_obj_list(coil_A.get_segments())
# current_object = bs.iobjs.CurrentObjList(coil_A.get_segments() + coil_B.get_segments())

current_object = bs.iobjs.CurrentObjList(coil_A.get_segments() + coil_B.get_segments())
#current_object = bs.iobjs.CurrentObjList(dual_coil.get_segments())

# make e field object
e_field = bs.eobjs.EField(amp_turns, current_object, velocity)

# calculate magnetic field at observation points
print("calculating B field")
B_obs = current_object.B(observation_points, amp_turns)

print("calculating E field")
E_obs = e_field.E(observation_points)


# E field line integral
# result = integrate.quad(sim_obj.E_integrand, 1, 1, args=(0, 1))
# print(result)

save(B_test=B_obs, E_test=E_obs, obs_points=observation_points)

if not enable_3D:
    plot_B('./data/B_test.npy')
    plot_E('./data/E_test.npy')

if enable_3D:
    plot_3D('./data/B_test.npy')


# octagonal_coil = bs.shapes.Octagon(0.8, 0.2, 0.2, 0.2, 0.1)

plot_coil(coil_A)
plot_coil(coil_B)
# plot_coil(octagonal_coil)

plt.show()
